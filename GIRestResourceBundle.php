<?php

namespace GI\RestResourceBundle;

use GI\RestResourceBundle\DependencyInjection\Compiler\GenerateManagerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class GIRestResourceBundle
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle
 */
class GIRestResourceBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new GenerateManagerPass());
    }

}
