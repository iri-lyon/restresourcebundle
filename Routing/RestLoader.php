<?php

namespace GI\RestResourceBundle\Routing;

use Doctrine\Common\Inflector\Inflector;
use GI\RestResourceBundle\Exception\InvalidArgumentException;
use GI\RestResourceBundle\Exception\InvalidResourceException;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceCollectionFactory;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceMetadataFactory;
use GI\RestResourceBundle\Metadata\RestResourceAction;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Config\Resource\DirectoryResource;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RestLoader
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Routing
 */
class RestLoader extends Loader
{

    const ROUTE_NAME_PREFIX = 'rest_';
    const DEFAULT_ACTION_PATTERN = 'rest.controller.default:';

    /**
     * @var AnnotationRestResourceCollectionFactory
     */
    protected $restResourceCollectionFactory;

    /**
     * @var AnnotationRestResourceMetadataFactory
     */
    protected $restResourceMetadataFactory;

    /**
     * @var string
     */
    protected $resourceClassDirectories;

    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * RestLoader constructor.
     *
     * @param AnnotationRestResourceCollectionFactory   $restResourceCollectionFactory
     * @param AnnotationRestResourceMetadataFactory     $restResourceMetadataFactory
     */
    public function __construct(
        AnnotationRestResourceCollectionFactory $restResourceCollectionFactory,
        AnnotationRestResourceMetadataFactory $restResourceMetadataFactory,
        $resourceClassDirectories
    ) {
        $this->restResourceCollectionFactory = $restResourceCollectionFactory;
        $this->restResourceMetadataFactory = $restResourceMetadataFactory;
        $this->resourceClassDirectories = $resourceClassDirectories;
    }

    /**
     * @param mixed $resource
     * @param null  $type
     *
     * @return RouteCollection
     *
     * @throws InvalidResourceException
     * @throws \Exception
     * @throws \GI\RestResourceBundle\Exception\ResourceClassNotFoundException
     */
    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routeCollection = new RouteCollection();
        foreach ($this->resourceClassDirectories as $directory) {
            $routeCollection->addResource(new DirectoryResource($directory, '/\.php$/'));
        }

        /** @var RestResourceMetadata $resource */
        foreach ($this->restResourceCollectionFactory->create() as $resourceClass) {
            $resourceMetadata = $this->restResourceMetadataFactory->create($resourceClass);
            $resourceName = $resourceMetadata->getName();

            if (null === $resourceName) {
                throw new InvalidResourceException(sprintf(
                    'Resource %s has no short name defined.',
                    $resourceClass
                ));
            }

            if (($collectionActions = $resourceMetadata->getCollectionActions()) !== null) {
                foreach ($collectionActions as $action) {
                    $this->addRoute(
                        $routeCollection,
                        $resourceClass,
                        $resourceName,
                        $action
                    );
                }
            }

            if (($itemActions = $resourceMetadata->getItemActions()) !== null) {
                foreach ($itemActions as $operationName => $action) {
                    $this->addRoute(
                        $routeCollection,
                        $resourceClass,
                        $resourceName,
                        $action
                    );
                }
            }
        }

        $this->loaded = true;

        return $routeCollection;
    }

    public function supports($resource, $type = null)
    {
        return $type === 'resource';
    }

    /**
     * @param RouteCollection    $routeCollection
     * @param string             $resourceClass
     * @param string             $resourceName
     * @param RestResourceAction $action
     *
     * @return Route
     */
    private function addRoute(
        RouteCollection $routeCollection,
        string $resourceClass,
        string $resourceName,
        RestResourceAction $action
    ) {

        if ($action->getRoute()) {
            return null;
        }

        if (!$action->getMethod()) {
            throw new \RuntimeException('Either a "route" or a "method" operation attribute must exist.');
        }

        if ($action->getController() === null) {
            if ($action->isCollection() && strtolower($action->getMethod()) == 'get') {
                $actionName = 'cgetAction';
            } elseif (in_array(strtolower($action->getMethod()), ['get', 'post', 'put', 'patch', 'delete'])) {
                $actionName = strtolower($action->getMethod()).'Action';
            } else {
                throw new InvalidArgumentException(sprintf(
                    'Operation %s for resource %s is not available',
                    $action->getMethod(),
                    $resourceName
                ));
            }

            $action->setController(self::DEFAULT_ACTION_PATTERN . $actionName);
        }

        $actionName = sprintf('%s_%s', strtolower($action->getMethod()), $action->getType());
        if ($action->getName() !== strtolower($action->getMethod())) {
            $actionName = sprintf('%s_%s', $action->getName(), $action->getType());
        }

        $resourceRouteName = Inflector::pluralize(Inflector::tableize($resourceName));
        $routeName = self::ROUTE_NAME_PREFIX.sprintf('%s_%s', $resourceRouteName, $actionName);

        if (!$action->getPath()) {
            $path = '/' . $resourceRouteName;
            if ($action->isItem()) {
                $path .= '/{id}';
            }
            $action->setPath($path);
        }

        $defaults = array_merge(
            $action->getDefaults(),
            [
                '_controller' => $action->getController(),
                '_format' => 'json',
                '_rest_resource_class' => $resourceClass,
                '_rest_resource_name' => $resourceName,
                sprintf('_rest_%s_action_name', $action->getType()) => $action->getName()
            ]
        );

        $route = new Route(
            $action->getPath(),
            $defaults,
            $action->getRequirements(), // requirements
            $action->getOptions(), // options
            '', // host
            [], // schemes
            [$action->getMethod()], // $methods
            $action->getCondition()
        );

        $routeCollection->add($routeName, $route);

        return $route;
    }
}
