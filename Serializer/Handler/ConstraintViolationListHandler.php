<?php

namespace GI\RestResourceBundle\Serializer\Handler;

use Doctrine\Common\Util\Inflector;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class MediaHandler
 * @package AppBundle\Serializer\Handler
 */
class ConstraintViolationListHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => ConstraintViolationList::class,
                'method' => 'serializeViolationListToJson',
            ),
        );
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param ConstraintViolationList  $constraintViolationList
     * @param array                    $type
     * @param Context                  $context
     *
     * @return array|string
     */
    public function serializeViolationListToJson(
        JsonSerializationVisitor $visitor,
        ConstraintViolationList $constraintViolationList,
        array $type,
        Context $context
    ) {

        $violations = [];
        $messages = [];

        foreach ($constraintViolationList as $violation) {
            $violations[] = [
                'property_path' => Inflector::tableize($violation->getPropertyPath()),
                'message' => $violation->getMessage(),
            ];

            $propertyPath = $violation->getPropertyPath();
            $prefix = $propertyPath ? sprintf('%s: ', $propertyPath) : '';

            $messages[] = $prefix.$violation->getMessage();
        }


        $data =  [
            'type' => 'https://tools.ietf.org/html/rfc2616#section-10',
            'title' => 'An error occurred',
            'detail' => $messages ? implode("\n", $messages) : (string) $constraintViolationList,
            'violations' => $violations,
        ];

        if (null === $visitor->getRoot()) {
            $visitor->setRoot($data);
        }

        return $data;
    }
}
