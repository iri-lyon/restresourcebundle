<?php

namespace GI\RestResourceBundle\Serializer;


use GI\RestResourceBundle\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;

class SerializerContextGroupsBuilder implements SerializerContextGroupsBuilderInterface
{
    use SerializerContextGroupsBuilderTrait;

    public function createFromRequest(Request $request, $subject, bool $normalization, array $extractedAttributes = null): array
    {

        $groups = [];

        $resourceName = $this->getShortName($extractedAttributes['resource_class']);

        $groups[] = 'always';
        $groups[] = $this->getGroupName('always', $resourceName);

        if ($normalization) {
            $groups[] = 'always_for_get';
            $groups[] = $this->getGroupName('always_for_get', $resourceName);
        } else {
            $groups[] = 'always_for_write';
            $groups[] = $this->getGroupName('always_for_write', $resourceName);
        }

        $groups[] = $this->getGroupName('method_' . strtolower($request->getMethod()), $resourceName);


        if (isset($extractedAttributes['collection_action_name'])) {
            if ($extractedAttributes['collection_action_name'] === 'get') {
                $groups[] = $this->getGroupName('list', $resourceName);
            } elseif ($extractedAttributes['collection_action_name'] === 'post' && !$normalization) {
                $groups[] = $this->getGroupName('write', $resourceName);
                $groups[] = $this->getGroupName('write_post', $resourceName);
            } elseif ($extractedAttributes['collection_action_name'] === 'post' && $normalization) {
                $groups[] = $this->getGroupName('details', $resourceName);
            }
        } elseif (isset($extractedAttributes['item_action_name'])) {
            if ($extractedAttributes['item_action_name'] === 'put' && !$normalization) {
                $groups[] = $this->getGroupName('write', $resourceName);
                $groups[] = $this->getGroupName('write_put', $resourceName);
            } elseif ($normalization) {
                $groups[] = $this->getGroupName('details', $resourceName);
            }
        }

        return array_unique($groups);
    }
}