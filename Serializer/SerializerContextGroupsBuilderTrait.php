<?php

namespace GI\RestResourceBundle\Serializer;

trait SerializerContextGroupsBuilderTrait
{
    /**
     * @param string $group
     * @param null   $prefix
     *
     * @return string
     */
    private function getGroupName($group, $prefix = null)
    {
        if ($prefix) {
            $group = $prefix . '.' .$group;
        }

        return $group;
    }

    /**
     * @return string
     */
    private function getShortName($class)
    {
        $reflexion = new \ReflectionClass($class);
        return strtolower($reflexion->getShortName());
    }

    /**
     * @return string
     */
    private function instanceOf($class, $implement)
    {
        $reflexion = new \ReflectionClass($class);
        return $reflexion->implementsInterface($implement);
    }
}
