<?php

namespace GI\RestResourceBundle\Tests\Controller;

use Doctrine\Common\Persistence\ManagerRegistry;
use GI\RestResourceBundle\Controller\AbstractRestController;
use GI\RestResourceBundle\Manager\AbstractResourceManager;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Security\AccessControlManager;
use GI\RestResourceBundle\Serializer\RequestDeserializerBuilder;
use GI\RestResourceBundle\Tests\Model\UserModel;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractRestControllerTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Controller
 */
class AbstractRestControllerTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
       $this->controllerBuilder = $this
           ->getMockBuilder(AbstractRestController::class)
           ->setConstructorArgs([
               $this->accessControlBuilder = $this->getMockBuilder(AccessControlManager::class)->disableOriginalConstructor()->getMock(),
               $this->getMockBuilder(TokenStorageInterface::class)->getMock(),
               $this->getMockBuilder(EventDispatcherInterface::class)->getMock(),
               $this->getMockBuilder(RequestDeserializerBuilder::class)->disableOriginalConstructor()->getMock(),
               $this->getMockBuilder(ValidatorInterface::class)->getMock()
           ])
           ->setMethods(null);

       $this->managerBuilder = $this
           ->getMockBuilder(AbstractResourceManager::class)
           ->setConstructorArgs([
               $this->getMockBuilder(ManagerRegistry::class)->getMock(),
               UserModel::class
           ]);
    }

    public function testSearch()
    {
        $user = new UserModel();
        $request = new Request();

        $controller = $this
            ->controllerBuilder
            ->getMock();

        $manager = $this->managerBuilder->getMock();
        $manager
            ->expects($this->once())
            ->method('findAll')
            ->willReturn([$user]);

        $this->accessControlBuilder
            ->expects($this->once())
            ->method('isGranted')
            ->with($request);//, $manager);


        $result = $controller->search($request, $manager, new RestResourceMetadata('UserModel'));

        $this->assertEquals([$user], $result);
    }

    public function testGetWithIdArg()
    {
       $user = new UserModel();
       $request = new Request();

       $controller = $this
           ->controllerBuilder
           ->getMock();

        $this->accessControlBuilder
            ->expects($this->once())
            ->method('isGranted')
           ->with($request, $user);

       $manager = $this->managerBuilder->getMock();
       $manager
           ->expects($this->once())
           ->method('find')
           ->with(10)
           ->willReturn($user);

       $manager
           ->expects($this->once())
           ->method('checkObject')
           ->with($user);


       $result = $controller->get($request, $manager, 10);

       $this->assertEquals($user, $result);
       $this->assertInstanceOf(UserModel::class, $result);
    }

    public function testGetWithObjectArg()
    {
        $user = new UserModel();
        $request = new Request();

        $controller = $this
            ->controllerBuilder
            ->getMock();

        $this->accessControlBuilder
            ->expects($this->once())
            ->method('isGranted')
            ->with($request, $user);

        $manager = $this->managerBuilder->getMock();
        $manager
            ->expects($this->never())
            ->method('find');

        $manager
            ->expects($this->once())
            ->method('checkObject')
            ->with($user);


        $result = $controller->get($request, $manager, $user);

        $this->assertEquals($user, $result);
        $this->assertInstanceOf(UserModel::class, $result);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testGetNotFound()
    {
        $request = new Request();

        $controller = $this
            ->controllerBuilder
            ->getMock();

        $this->accessControlBuilder
            ->expects($this->never())
            ->method('isGranted');

        $manager = $this->managerBuilder->getMock();
        $manager
            ->expects($this->once())
            ->method('find')
            ->with(10)
            ->willReturn(null);

        $manager
            ->expects($this->never())
            ->method('checkObject');


        $controller->get($request, $manager, 10);
    }
}
