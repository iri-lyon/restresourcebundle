<?php

namespace GI\RestResourceBundle\Tests\Routing;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Inflector\Inflector;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceCollectionFactory;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceMetadataFactory;
use GI\RestResourceBundle\Metadata\RestResourceAction;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Routing\RestLoader;
use GI\RestResourceBundle\Tests\Model\UserModel;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RestLoaderTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Routing
 */
class RestLoaderTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        $this->restResourceCollectionFactoryBuilder = $this
            ->getMockBuilder(AnnotationRestResourceCollectionFactory::class)
            ->setConstructorArgs([
                new AnnotationReader(),
                []
            ])
            ->setMethods(null);

        $this->restResourceMetadataFactoryBuilder = $this
            ->getMockBuilder(AnnotationRestResourceMetadataFactory::class)
            ->setConstructorArgs([
                new AnnotationReader()
            ])
            ->setMethods(null);

        $this->routerBuilder = $this
            ->getMockBuilder(RestLoader::class)
            ->setMethods(null);
    }

    public function testLoad()
    {
        $metadataFactory = $this
            ->restResourceMetadataFactoryBuilder
            ->setMethods(['create'])
            ->getMock();
        $metadataFactory
            ->expects($this->once())
            ->method('create')
            ->with(UserModel::class)
            ->willReturn(new RestResourceMetadata(
                'UserModel',
                'app.usermodel.manager',
                ['get' => new RestResourceAction('get', RestResourceAction::TYPE_COLLECTION)]
            ));

        $collectionFactory = $this
            ->restResourceCollectionFactoryBuilder
            ->setConstructorArgs([
                new AnnotationReader(),
                [UserModel::class]
            ])
            ->setMethods(['create'])
            ->getMock();

        $collectionFactory
            ->expects($this->once())
            ->method('create')
            ->willReturn([UserModel::class]);

        $loader = $this->routerBuilder
            ->setConstructorArgs([
                $collectionFactory,
                $metadataFactory
            ])
            ->getMock();

        /** @var RouteCollection $collection */
        $collection = $loader->load(null);

        $this->assertEquals(1, $collection->count());
    }

    public function testSupports()
    {
        $loader = $this
            ->getMockBuilder(RestLoader::class)
            ->disableOriginalConstructor()
            ->setMethods(null)
            ->getMock();

        $this->assertTrue($loader->supports(null, 'resource'));
    }

    /**
     * @dataProvider getValidActions
     */
    public function testAddRoute(RestResourceAction $action, $path, $routeName)
    {
        $loader = $this
            ->getMockBuilder(RestLoader::class)
            ->disableOriginalConstructor()
            ->setMethods(null)
            ->getMock();

        $collection = new RouteCollection();
        /** @var Route $route */
        $route = $this->invokeMethod($loader, 'addRoute', [
            $collection,
            UserModel::class,
            'UserModel',
            $action
        ]);

        $options = array_merge(
            [ 'compiler_class' => 'Symfony\Component\Routing\RouteCompiler'],
            $action->getOptions()
        );

        $defaults = array_merge(
            [
                '_format' => 'json',
                '_controller' => $action->getController(),
                '_rest_resource_class' => UserModel::class,
                '_rest_resource_name' => 'UserModel',
                sprintf('_rest_%s_action_name', $action->getType()) => $action->getName()
            ],
            $action->getDefaults()
        );

        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals([$action->getMethod()], $route->getMethods());
        $this->assertEquals($action->getRequirements(), $route->getRequirements());
        $this->assertEquals($defaults, $route->getDefaults());
        $this->assertEquals($options, $route->getOptions());
        $this->assertEquals($action->getCondition(), $route->getCondition());
        $this->assertEquals($path, $route->getPath());

        $this->assertInstanceOf(Route::class, $collection->get($routeName));
        $this->assertEquals($route, $collection->get($routeName));
    }

    public function testAddRouteWithRoute()
    {
        $loader = $this
            ->getMockBuilder(RestLoader::class)
            ->disableOriginalConstructor()
            ->setMethods(null)
            ->getMock();

        $collection = new RouteCollection();

        /** @var Route $route */
        $route = $this->invokeMethod($loader, 'addRoute', [
            $collection,
            UserModel::class,
            'UserModel',
            new RestResourceAction('get', RestResourceAction::TYPE_ITEM, null, null, null , 'my_route')
        ]);

        $this->assertNull($route);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function getValidActions()
    {
        return [
            [
                new RestResourceAction('get', RestResourceAction::TYPE_COLLECTION),
                '/user_models',
                'api_user_models_get_collection'
            ],
            [
                new RestResourceAction('post', RestResourceAction::TYPE_COLLECTION),
                '/user_models',
                'api_user_models_post_collection'
            ],
            [
                new RestResourceAction('get', RestResourceAction::TYPE_ITEM),
                '/user_models/{id}',
                'api_user_models_get_item'
            ],
            [
                new RestResourceAction('put', RestResourceAction::TYPE_ITEM),
                '/user_models/{id}',
                'api_user_models_put_item'
            ],
            [
                new RestResourceAction('delete', RestResourceAction::TYPE_ITEM),
                '/user_models/{id}',
                'api_user_models_delete_item'
            ],
            [
                new RestResourceAction(
                    'custom',
                    RestResourceAction::TYPE_ITEM,
                    'GET',
                    '/user_models/{id}/custom'
                ),
                '/user_models/{id}/custom',
                'api_user_models_custom_item'
            ],
            [
                new RestResourceAction(
                    'controller',
                    RestResourceAction::TYPE_ITEM,
                    'PUT',
                    '/user_models/{id}/controller',
                    'my.custom.controller'
                ),
                '/user_models/{id}/controller',
                'api_user_models_controller_item'
            ]
        ];
    }
}
