<?php

namespace GI\RestResourceBundle\Tests\Metadata\Factory;

use Doctrine\Common\Annotations\AnnotationReader;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceCollectionFactory;
use GI\RestResourceBundle\Metadata\RestResourceCollection;
use GI\RestResourceBundle\Tests\Model\UserModel;

/**
 * Class AnnotationRestResourceCollectionFactoryTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Metadata\Factory
 */
class AnnotationRestResourceCollectionFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testCreate()
    {
        $factory = new AnnotationRestResourceCollectionFactory(new AnnotationReader(), [UserModel::class, '\\Data\\Not\\Found\\Class']);

        $resources = $factory->create();

        $this->assertInstanceOf(RestResourceCollection::class, $resources);
        $this->assertEquals([UserModel::class], array_values($resources->getIterator()->getArrayCopy()));
    }
}
