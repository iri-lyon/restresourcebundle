<?php

namespace GI\RestResourceBundle\Tests\Metadata\Factory;

use Doctrine\Common\Annotations\AnnotationReader;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceMetadataFactory;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Metadata\RestResourcePropertyMetadata;
use GI\RestResourceBundle\Tests\Model\UserModel;
use GI\RestResourceBundle\Tests\Model\UserModelNotResource;

/**
 * Class AnnotationRestResourceMetadataFactoryTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Metadata\Factory
 */
class AnnotationRestResourceMetadataFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreate()
    {
        $factory = new AnnotationRestResourceMetadataFactory(new AnnotationReader(), 'app.%s.manager');

        $metadata = $factory->create(UserModel::class);

        $this->assertEquals('app.usermodel.manager', $metadata->getManager());
        $this->assertEquals('UserModel', $metadata->getName());


        $this->assertEquals(['get', 'put', 'delete'], array_keys($metadata->getItemActions()));
        $this->assertEquals(['get', 'post'], array_keys($metadata->getCollectionActions()));
        $this->assertEquals([], array_keys($metadata->getAttributes()));
        $this->assertInstanceOf(RestResourceMetadata::class, $metadata);

        $this->assertEquals(2, count($metadata->getProperties()));

        $id = $metadata->getProperty('id');
        $this->assertInstanceOf(RestResourcePropertyMetadata::class, $id);

        $this->assertFalse($id->hasFilter('match'));
        $this->assertFalse($id->hasFilter('order'));
        $this->assertFalse($id->hasFilter('filter'));

        $name = $metadata->getProperty('name');
        $this->assertInstanceOf(RestResourcePropertyMetadata::class, $name);

        $this->assertTrue($name->hasFilter('match'));
        $this->assertTrue($name->hasFilter('order'));
        $this->assertFalse($name->hasFilter('filter'));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Resource "UserModel" has no manager
     */
    public function testCreateWithOutManager()
    {
        $factory = new AnnotationRestResourceMetadataFactory(new AnnotationReader());

        $factory->create(UserModel::class);
    }

    /**
     * @expectedException \GI\RestResourceBundle\Exception\ResourceClassNotFoundException
     */
    public function testCreateNotExists()
    {
        $factory = new AnnotationRestResourceMetadataFactory(new AnnotationReader());

        $factory->create('\\Not\\Found');
    }

    /**
     * @expectedException \Exception
     */
    public function testCreateNoAnnotationInModel()
    {
        $factory = new AnnotationRestResourceMetadataFactory(new AnnotationReader());

        $factory->create(UserModelNotResource::class);
    }
}
