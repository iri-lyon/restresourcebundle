<?php

namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResource;

/**
 * Class RestResourcetest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Annotation
 */
class RestResourceTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
        $this->resource = new RestResource();
    }

    public function testDefaultValues()
    {
        $this->assertEquals(null, $this->resource->name);
        $this->assertEquals(null, $this->resource->manager);
        $this->assertEquals([], $this->resource->attributes);
    }

    public function testNameGetter()
    {
        $this->resource->name = 'fooFoo';
        $this->assertEquals('fooFoo', $this->resource->getName());

    }

    public function testManagerGetter()
    {
        $this->resource->manager = 'BarManager';
        $this->assertEquals('BarManager', $this->resource->getManager());
    }
}
