<?php
namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResourceFilter;
use GI\RestResourceBundle\Annotation\RestResourceFilterMatch;
use GI\RestResourceBundle\Annotation\RestResourceFilterOrderBy;

/**
 * Class RestResourceFilterOrderByTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Annotation
 */
class RestResourceFilterOrderByTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultValues()
    {
        $resource = new RestResourceFilterOrderBy();
        $this->assertEquals(RestResourceFilter::TYPE_FILTER_ORDER_BY, $resource->type);
    }
}
