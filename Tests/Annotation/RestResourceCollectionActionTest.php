<?php
namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResourceAction;
use GI\RestResourceBundle\Annotation\RestResourceCollectionAction;

/**
 * Class RestResourceCollectionActionTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Annotation
 */
class RestResourceCollectionActionTest extends \PHPUnit_Framework_TestCase
{

    public function testDefaultValues()
    {
        $resource = new RestResourceCollectionAction(['value' => 'foo']);

        $this->assertEquals(RestResourceAction::TYPE_COLLECTION, $resource->getType());
    }
}
