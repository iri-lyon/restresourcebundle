<?php
namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResourceFilter;
use GI\RestResourceBundle\Annotation\RestResourceFilterMatch;

/**
 * Class RestResourceFilterMatchTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Annotation
 */
class RestResourceFilterMatchTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultValues()
    {
        $resource = new RestResourceFilterMatch();
        $this->assertEquals(RestResourceFilter::TYPE_FILTER_MATCH, $resource->type);
    }
}
