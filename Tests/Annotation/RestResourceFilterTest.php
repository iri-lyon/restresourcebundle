<?php
namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResourceFilter;

class RestResourceFilterTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultValues()
    {
        $resource = new RestResourceFilter();
        $this->assertEquals(RestResourceFilter::TYPE_FILTER, $resource->type);
    }
}
