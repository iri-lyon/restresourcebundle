<?php
namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResourceAction;

/**
 * Class RestResourceActionTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Annotation
 */
class RestResourceActionTest extends \PHPUnit_Framework_TestCase
{

    public function testDefaultValues()
    {
        $resource = new RestResourceAction(['value' => 'foo']);

        $this->assertEquals('foo', $resource->getName());
        $this->assertEquals(null, $resource->getPath());
        $this->assertEquals(null, $resource->getType());
        $this->assertEquals(null, $resource->getController());
        $this->assertEquals(null, $resource->getRoute());
        $this->assertEquals([], $resource->getRequirements());
        $this->assertEquals([], $resource->getOptions());
        $this->assertEquals([], $resource->getDefaults());
        $this->assertEquals(null, $resource->getCondition());
        $this->assertEquals(null, $resource->getDescription());
        $this->assertEquals(null, $resource->getValidationGroups());
        $this->assertEquals(null, $resource->getSecurity());
    }

    /**
     * @dataProvider getValidParameters
     */
    public function testActionParameters($parameter, $value, $getter)
    {
        $route = new RestResourceAction([$parameter => $value]);
        $this->assertEquals($route->$getter(), $value);
    }

    public function getValidParameters()
    {
        return [
            ['value', 'get', 'getName'],
            ['type', 'item', 'getType'],
            ['type', 'collection', 'getType'],
            ['controller', 'app.controller.bar', 'getController'],
            ['path', '/MyPath', 'getPath'],
            ['requirements', ['version' => '3.0'], 'getRequirements'],
            ['options', ['my_option' => 'MyOptionValue'], 'getOptions'],
            ['name', 'post', 'getName'],
            ['defaults', ['_attributes' => 'MyAttribute'], 'getDefaults'],
            ['condition', 'request.attributes.get("version") == 1.0', 'getCondition'],
            ['description', 'Action description', 'getDescription'],
            ['route', 'my_route', 'getRoute'],
            ['security', 'is_granted("ROLE_USER")', 'getSecurity']
        ];
    }
}
