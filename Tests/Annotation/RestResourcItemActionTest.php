<?php
namespace GI\RestResourceBundle\Tests\Annotation;

use GI\RestResourceBundle\Annotation\RestResourceAction;
use GI\RestResourceBundle\Annotation\RestResourceItemAction;

/**
 * Class RestResourceItemActionTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Annotation
 */
class RestResourceItemActionTest extends \PHPUnit_Framework_TestCase
{

    public function testDefaultValues()
    {
        $resource = new RestResourceItemAction(['value' => 'foo']);

        $this->assertEquals(RestResourceAction::TYPE_ITEM, $resource->getType());
    }
}
