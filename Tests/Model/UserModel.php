<?php

namespace GI\RestResourceBundle\Tests\Model;

use GI\RestResourceBundle\Annotation\RestResource;
use GI\RestResourceBundle\Annotation\RestResourceCollectionAction;
use GI\RestResourceBundle\Annotation\RestResourceFilterMatch;
use GI\RestResourceBundle\Annotation\RestResourceFilterOrderBy;
use GI\RestResourceBundle\Annotation\RestResourceItemAction;
use GI\RestResourceBundle\Annotation\RestResourceSecurity;
use GI\RestResourceBundle\Annotation\RestResourceSecurityRoles;

/**
 * Class UserModel
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Model
 *
 * @RestResource()
 *
 * @RestResourceItemAction("get", description="User", security="is_granted('ROLE_ADMIN')")
 * @RestResourceItemAction("put")
 * @RestResourceItemAction("delete")
 *
 * @RestResourceCollectionAction("get")
 * @RestResourceCollectionAction("post")
 */
class UserModel
{

    private $id;

    /**
     * @var string
     *
     * @RestResourceFilterMatch()
     * @RestResourceFilterOrderBy()
     */
    private $name;

    /**
     * UserModel constructor.
     *
     * @param $id
     * @param $name
     */
    public function __construct($id = null, $name = null)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return UserModel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return UserModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
