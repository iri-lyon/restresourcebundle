<?php

namespace GI\RestResourceBundle\Tests\Model;

/**
 * Class UserModel
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\Model
 *
 */
class UserModelNotResource
{

    private $id;

    private $name;

    /**
     * UserModel constructor.
     *
     * @param $id
     * @param $name
     */
    public function __construct($id = null, $name = null)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return UserModel
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return UserModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


}
