<?php

namespace GI\RestResourceBundle\Tests\EventListener;

use Doctrine\Common\Annotations\AnnotationReader;
use GI\RestResourceBundle\EventListener\RequestInjectorListener;
use GI\RestResourceBundle\Manager\AbstractResourceManager;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceMetadataFactory;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Tests\Model\UserModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class ReadListenerTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\EventListener
 */
class RequestInjectorListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testOnKernelRequest()
    {
        $request = new Request();
        $manager = $this->getMockBuilder(AbstractResourceManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $factory = $this
            ->getMockBuilder(AnnotationRestResourceMetadataFactory::class)
            ->setConstructorArgs([
                new AnnotationReader()
            ])
            ->getMock();

        $factory
            ->expects($this->once())
            ->method('create')
            ->willReturn(new RestResourceMetadata(
                'UserModel',
                'app.user.manager'
            ));

        $request->attributes->set('_rest_resource_class', UserModel::class);
        $request->attributes->set('_rest_resource_name', 'UserModel');
        $request->attributes->set('_rest_collection_action_name', 'get');

        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')->getMock();
        $container
            ->expects($this->exactly(2))
            ->method('get')
            ->with($this->logicalOr(
                'rest.metadata.rest_resource.metadata_factory',
                'app.user.manager'
            ))
            ->will($this->returnCallback(function($arg) use ($factory, $manager) {
                if ($arg === 'rest.metadata.rest_resource.metadata_factory') {
                    return $factory;
                } else {
                    return $manager;
                }
            }));

        $container
            ->expects($this->once())
            ->method('has')
            ->with('app.user.manager')
            ->willReturn($manager);

        $injector = new RequestInjectorListener($container);
        $event = $this->getMockBuilder(GetResponseEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())
            ->method('getRequest')
            ->will($this->returnValue($request));

        $injector->onKernelRequest($event);

        $this->assertEquals($request->attributes->get('resourceMetadata')->getManager(), 'app.user.manager');
        $this->assertEquals($request->attributes->get('resourceMetadata')->getName(), 'UserModel');
        $this->assertEquals($request->attributes->get('manager'), $manager);
    }

    public function testOnKernelRequestWithoutResource()
    {
        $request = new Request();

        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')->getMock();
        $container
            ->expects($this->never())
            ->method('get');

        $injector = new RequestInjectorListener($container);
        $event = $this->getMockBuilder(GetResponseEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())
            ->method('getRequest')
            ->will($this->returnValue($request));

        $injector->onKernelRequest($event);

        $this->assertEquals($request->attributes->get('resourceMetadata'), null);
        $this->assertEquals($request->attributes->get('manager'), null);
    }
}
