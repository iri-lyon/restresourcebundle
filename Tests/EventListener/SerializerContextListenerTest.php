<?php

namespace GI\RestResourceBundle\Tests\EventListener;

use FOS\RestBundle\View\ViewHandler;
use GI\RestResourceBundle\EventListener\SerializerContextListener;
use GI\RestResourceBundle\Serializer\SerializerContextGroupsBuilderInterface;
use GI\RestResourceBundle\Tests\Model\UserModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Form\FormInterface;

/**
 * Class SerializerContextListenerTest
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Tests\EventListener
 */
class SerializerContextListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testOnKernelView()
    {
        $request = new Request([], [], [
            '_rest_resource_class' => UserModel::class,
            '_rest_resource_name' => 'UserModel',
            '_rest_collection_action_name' => 'get'
        ]);
        $data = new UserModel(10, 'doe');

        /** @var ViewHandler $viewHandler */
        $viewHandler = $this->getMockBuilder(ViewHandler::class)
            ->disableOriginalConstructor()
            ->getMock();

        $viewHandler
            ->expects($this->once())
            ->method('setExclusionStrategyGroups')
            ->with(['mygroups', 'myothergroup']);

        /** @var SerializerContextGroupsBuilderInterface $serialierContext */
        $serialierContext = $this->getMockBuilder(SerializerContextGroupsBuilderInterface::class)
            ->getMock();

        $serialierContext
            ->expects($this->once())
            ->method('createFromRequest')
            ->with($request, $data, true)
            ->willReturn(['mygroups', 'myothergroup']);

        $listener = new SerializerContextListener($viewHandler, $serialierContext);
        $event = $this->getMockBuilder(GetResponseForControllerResultEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())
            ->method('getRequest')
            ->will($this->returnValue($request));

        $event->expects($this->once())
            ->method('getControllerResult')
            ->will($this->returnValue($data));


        $listener->onKernelView($event);
    }


    public function testOnKernelViewWithoutResource()
    {
        $request = new Request([], [], []);

        /** @var ViewHandler $viewHandler */
        $viewHandler = $this->getMockBuilder(ViewHandler::class)
            ->disableOriginalConstructor()
            ->getMock();

        $viewHandler
            ->expects($this->never())
            ->method('setExclusionStrategyGroups');

        /** @var SerializerContextGroupsBuilderInterface $serialierContext */
        $serialierContext = $this->getMockBuilder(SerializerContextGroupsBuilderInterface::class)
            ->getMock();

        $serialierContext
            ->expects($this->never())
            ->method('createFromRequest');

        $listener = new SerializerContextListener($viewHandler, $serialierContext);
        $event = $this->getMockBuilder(GetResponseForControllerResultEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())
            ->method('getRequest')
            ->will($this->returnValue($request));

        $event->expects($this->once())
            ->method('getControllerResult')
            ->will($this->returnValue([]));


        $listener->onKernelView($event);
    }

    public function testOnKernelViewWithFormData()
    {
        $request = new Request([], [], [
            '_rest_resource_class' => UserModel::class,
            '_rest_resource_name' => 'UserModel',
            '_rest_collection_action_name' => 'get'
        ]);
        $data = $this->getMockBuilder(FormInterface::class)->getMock();

        /** @var ViewHandler $viewHandler */
        $viewHandler = $this->getMockBuilder(ViewHandler::class)
            ->disableOriginalConstructor()
            ->getMock();

        $viewHandler
            ->expects($this->never())
            ->method('setExclusionStrategyGroups');

        /** @var SerializerContextGroupsBuilderInterface $serialierContext */
        $serialierContext = $this->getMockBuilder(SerializerContextGroupsBuilderInterface::class)
            ->getMock();

        $serialierContext
            ->expects($this->never())
            ->method('createFromRequest');

        $listener = new SerializerContextListener($viewHandler, $serialierContext);
        $event = $this->getMockBuilder(GetResponseForControllerResultEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())
            ->method('getRequest')
            ->will($this->returnValue($request));

        $event->expects($this->once())
            ->method('getControllerResult')
            ->will($this->returnValue($data));


        $listener->onKernelView($event);
    }
}
