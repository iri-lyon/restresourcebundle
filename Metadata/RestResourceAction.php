<?php

namespace GI\RestResourceBundle\Metadata;

/**
 * Class RestResourceAction
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Metadata
 */
class RestResourceAction
{
    const TYPE_ITEM = 'item';
    const TYPE_COLLECTION = 'collection';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $route;

    /**
     * @var array
     */
    protected $requirements = [];

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var array
     */
    protected $defaults = [];

    /**
     * @var string
     */
    protected $condition;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var callable|array
     */
    protected $validationGroups;

    /**
     * @var string|null
     */
    protected $security;

    /**
     * RestResourceAction constructor.
     *
     * @param string                $name
     * @param string                $type
     * @param string|null           $method
     * @param string|null           $path
     * @param string|null           $controller
     * @param string|null           $route
     * @param array|null            $requirements
     * @param array|null            $options
     * @param array|null            $defaults
     * @param string|null           $condition
     * @param string|null           $description
     * @param callable|array|null   $validationGroups
     * @param string|null            $security
     */
    public function __construct(
        string $name,
        string $type,
        string $method = null,
        string $path = null,
        string $controller = null,
        string $route = null,
        array $requirements = [],
        array $options = [],
        array $defaults = [],
        string $condition = null,
        string $description = null,
        $validationGroups = null,
        string $security = null
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->method = $method;
        $this->path = $path;
        $this->controller = $controller;
        $this->route = $route;
        $this->requirements = $requirements;
        $this->options = $options;
        $this->defaults = $defaults;
        $this->condition = $condition;
        $this->description = $description;
        $this->validationGroups = $validationGroups;
        $this->security = $security;

        if ($this->method === null && in_array($this->name, ['get','post','put','patch','delete'])) {
            $this->method = strtoupper($this->name);
        }

        if ($this->method === null) {
            throw new \InvalidArgumentException("Action method is null");
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return RestResourceAction
     */
    public function setName(string $name): RestResourceAction
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return RestResourceAction
     */
    public function setMethod(string $method): RestResourceAction
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return RestResourceAction
     */
    public function setPath(string $path): RestResourceAction
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getController(): ?string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     *
     * @return RestResourceAction
     */
    public function setController(string $controller): RestResourceAction
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string $route
     *
     * @return RestResourceAction
     */
    public function setRoute(string $route): RestResourceAction
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequirements(): ?array
    {
        return $this->requirements;
    }

    /**
     * @param array $requirements
     *
     * @return RestResourceAction
     */
    public function setRequirements(array $requirements): RestResourceAction
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return RestResourceAction
     */
    public function setOptions(array $options): RestResourceAction
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getDefaults(): ?array
    {
        return $this->defaults;
    }

    /**
     * @param array $defaults
     *
     * @return RestResourceAction
     */
    public function setDefaults(array $defaults): RestResourceAction
    {
        $this->defaults = $defaults;

        return $this;
    }

    /**
     * @return string
     */
    public function getCondition(): ?string
    {
        return $this->condition;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return RestResourceAction
     */
    public function setDescription(string $description): RestResourceAction
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array|callable
     */
    public function getValidationGroups()
    {
        return $this->validationGroups;
    }

    /**
     * @param array|callable $validationGroups
     *
     * @return RestResourceAction
     */
    public function setValidationGroups($validationGroups)
    {
        $this->validationGroups = $validationGroups;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSecurity(): ?string
    {
        return $this->security;
    }

    /**
     * @param string|null $security
     *
     * @return RestResourceAction
     */
    public function setSecurity(?string $security): RestResourceAction
    {
        $this->security = $security;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCollection(): bool
    {
        return $this->type === self::TYPE_COLLECTION;
    }

    /**
     * @return bool
     */
    public function isItem(): bool
    {
        return $this->type === self::TYPE_ITEM;
    }
}
