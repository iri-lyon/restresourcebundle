<?php

namespace GI\RestResourceBundle\Metadata;

/**
 * Class ResourceMetadata
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Metadata
 */
class RestResourceMetadata
{

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string
     */
    private $manager;

    /**
     * @var array|null
     */
    private $collectionActions;

    /**
     * @var array|null
     */
    private $itemActions;

    /**
     * @var array|null
     */
    public $attributes;

    /**
     * @var array|null
     */
    public $properties;

    /**
     * RestResourceMetadata constructor.
     *
     * @param string      $name
     * @param string|null $manager
     * @param array|null  $collectionActions
     * @param array|null  $itemActions
     * @param array|null  $attributes
     * @param array|null  $properties
     */
    public function __construct(
        string $name,
        string $manager = null,
        array $collectionActions = null,
        array $itemActions = null,
        array $attributes = null,
        array $properties = null
    ) {
        $this->name = $name;
        $this->manager = $manager;
        $this->collectionActions = $collectionActions;
        $this->itemActions = $itemActions;
        $this->attributes = $attributes;
        $this->properties = $properties;
    }

    /**
     * @return string
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param string $manager
     *
     * @return RestResourceMetadata
     */
    public function setManager(string $manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     *
     * @return RestResourceMetadata
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getCollectionActions()
    {
        return $this->collectionActions;
    }

    /**
     * @param array|null $collectionActions
     *
     * @return RestResourceMetadata
     */
    public function setCollectionActions($collectionActions)
    {
        $this->collectionActions = $collectionActions;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getItemActions()
    {
        return $this->itemActions;
    }

    /**
     * @param array|null $itemActions
     *
     * @return RestResourceMetadata
     */
    public function setItemActions($itemActions)
    {
        $this->itemActions = $itemActions;

        return $this;
    }

    /**
     * Gets a collection operation attribute, optionally fallback to a resource attribute.
     *
     * @param string $operationName
     * @param string $key
     * @param mixed  $defaultValue
     * @param bool   $resourceFallback
     *
     * @return mixed
     */
    public function getCollectionActionAttribute(
        string $operationName,
        string $key,
        $defaultValue = null,
        bool $resourceFallback = false
    ) {
        return $this->getActionAttribute(
            $this->collectionActions,
            $operationName,
            $key,
            $defaultValue,
            $resourceFallback
        );
    }

    /**
     * Gets an item operation attribute, optionally fallback to a resource attribute.
     *
     * @param string $operationName
     * @param string $key
     * @param mixed  $defaultValue
     * @param bool   $resourceFallback
     *
     * @return mixed
     */
    public function getItemActionAttribute(
        string $operationName,
        string $key,
        $defaultValue = null,
        bool $resourceFallback = false
    ) {
        return $this->getActionAttribute(
            $this->itemActions,
            $operationName,
            $key,
            $defaultValue,
            $resourceFallback
        );
    }

    /**
     * Gets an operation attribute, optionally fallback to a resource attribute.
     *
     * @param array|null $operations
     * @param string     $operationName
     * @param string     $key
     * @param mixed      $defaultValue
     * @param bool       $resourceFallback
     *
     * @return mixed
     */
    private function getActionAttribute(
        array $operations = null,
        string $operationName,
        string $key,
        $defaultValue = null,
        bool $resourceFallback = false
    ) {
        if (isset($operations[$operationName]) &&
            method_exists($operations[$operationName], $method = 'get'.str_replace('_', '', $key)) &&
            ($result = $operations[$operationName]->$method()) !== null
        ) {
            return $result;
        }

        if ($resourceFallback && isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }

        return $defaultValue;
    }

    /**
     * @return array|null
     */
    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    /**
     * @param array|null $attributes
     *
     * @return RestResourceMetadata
     */
    public function setAttributes($attributes): RestResourceMetadata
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed|null
     */
    public function getAttribute($key, $default = null)
    {
        if (isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }

        return $default;
    }

    /**
     * @return array|null
     */
    public function getProperties(): ?array
    {
        return $this->properties;
    }

    /**
     * @param array|null $properties
     *
     * @return RestResourceMetadata
     */
    public function setProperties(?array $properties): RestResourceMetadata
    {
        $this->properties = $properties;

        return $this;
    }

    /**
     * @param $name
     *
     * @return RestResourcePropertyMetadata|null
     */
    public function getProperty($name) : ?RestResourcePropertyMetadata
    {
        if (isset($this->properties[$name])) {
            return $this->properties[$name];
        }

        return null;
    }
}
