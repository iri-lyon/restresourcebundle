<?php

namespace GI\RestResourceBundle\Metadata;

/**
 * Class RestResourceProperty
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Metadata
 */
class RestResourcePropertyMetadata
{

    /**
     * @var array
     */
    private $filters;

    /**
     * RestResourcePropertyMetadata constructor.
     *
     * @param array $filters
     */
    public function __construct(array $filters = [])
    {
        $this->filters = $filters;
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function hasFilter($type)
    {
        return in_array($type, $this->filters);
    }
}
