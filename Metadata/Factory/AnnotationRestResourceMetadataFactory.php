<?php

namespace GI\RestResourceBundle\Metadata\Factory;

use Doctrine\Common\Annotations\Reader;
use GI\RestResourceBundle\Annotation\RestResource;
use GI\RestResourceBundle\Annotation\RestResourceFilter;
use GI\RestResourceBundle\Exception\ResourceClassNotFoundException;
use GI\RestResourceBundle\Metadata\RestResourceAction;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Annotation\RestResourceAction as RestResourceActionAnnotation;
use GI\RestResourceBundle\Metadata\RestResourcePropertyMetadata;

/**
 * Class AnnotationRestResourceMetadataFactory
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Metadata\Factory
 */
class AnnotationRestResourceMetadataFactory
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var string
     */
    private $autoGenerateManagerName;

    /**
     * @var array
     */
    private $resources = [];

    /**
     * AnnotationRestResourceCollectionFactory constructor.
     *
     * @param Reader $reader
     * @param string $autoGenerateManagerName
     */
    public function __construct(Reader $reader, $autoGenerateManagerName = null)
    {
        $this->reader = $reader;
        $this->autoGenerateManagerName = $autoGenerateManagerName;
    }

    /**
     * @param string $resourceClass
     *
     * @return RestResourceMetadata
     *
     * @throws ResourceClassNotFoundException
     * @throws \Exception
     */
    public function create(string $resourceClass): RestResourceMetadata
    {

        if (isset($this->resources[$resourceClass])) {
            return $this->resources[$resourceClass];
        }

        try {
            $reflectionClass = new \ReflectionClass($resourceClass);
        } catch (\ReflectionException $reflectionException) {
            throw new ResourceClassNotFoundException(sprintf('Resource "%s" not found.', $resourceClass));
        }

        /** @var RestResource $resourceAnnotation */
        $resourceAnnotation = $this->reader->getClassAnnotation($reflectionClass, RestResource::class);
        if ($resourceAnnotation === null) {
            throw new \Exception(sprintf(
                'Resource annotation for "%s" not found.',
                $resourceClass
            ));
        }

        $actions = [
            RestResourceAction::TYPE_COLLECTION => [],
            RestResourceAction::TYPE_ITEM => []
        ];

        $annotations = $this->reader->getClassAnnotations($reflectionClass);
        foreach ($annotations as $annotation) {
            if ($annotation instanceof RestResourceActionAnnotation) {
                if (isset($actions[$annotation->getType()][$annotation->getName()])) {
                    throw new \InvalidArgumentException(sprintf(
                        'Action "%s" exists for %s %s',
                        $annotation->getName(),
                        $annotation->getType(),
                        $reflectionClass->getName()
                    ));
                }

                $actions[$annotation->getType()][$annotation->getName()] = $this->createAction($annotation);
            }
        }

        $properties = [];
        foreach ($reflectionClass->getProperties() as $property) {
            $annotations = $this->reader->getPropertyAnnotations($property);
            $filters = [];
            foreach ($annotations as $annotation) {
                if ($annotation instanceof RestResourceFilter) {
                    $filters[] = $annotation->type;
                }
            }
            $properties[$property->getName()] = new RestResourcePropertyMetadata($filters);
        }

        return $this->resources[$resourceClass] = $this->createMetadata(
            $resourceAnnotation,
            $resourceClass,
            $actions,
            $properties
        );
    }

    /**
     * @param RestResource $annotation
     * @param string       $resourceClass
     * @param array        $actions
     *
     * @return RestResourceMetadata
     */
    public function createMetadata(
        RestResource $annotation,
        string $resourceClass,
        array $actions,
        array $properties
    ): RestResourceMetadata {

        if ($annotation->name === null) {
            $annotation->name = substr($resourceClass, strrpos($resourceClass, '\\') + 1);
        }

        if ($annotation->manager === null) {
            if ($this->autoGenerateManagerName !== null) {
                $annotation->manager = sprintf(
                    $this->autoGenerateManagerName,
                    strtolower($annotation->name)
                );
            } else {
                throw new \InvalidArgumentException(sprintf(
                    'Resource "%s" has no manager',
                    $annotation->name
                ));
            }
        }

        $collectionActions = $actions[RestResourceAction::TYPE_COLLECTION];
        if (empty($collectionActions)) {
            $collectionActions = [
                'get' => $this->createDefaultAction(RestResourceAction::TYPE_COLLECTION, 'get'),
                'post' => $this->createDefaultAction(RestResourceAction::TYPE_COLLECTION, 'post')
            ];
        }

        $itemActions = $actions[RestResourceAction::TYPE_ITEM];
        if (empty($itemActions)) {
            $itemActions = [
                'get' => $this->createDefaultAction(RestResourceAction::TYPE_ITEM, 'get'),
                'put' => $this->createDefaultAction(RestResourceAction::TYPE_ITEM, 'put'),
                'delete' => $this->createDefaultAction(RestResourceAction::TYPE_ITEM, 'delete')
            ];
        }

        return new RestResourceMetadata(
            $annotation->name,
            $annotation->manager,
            $collectionActions,
            $itemActions,
            $annotation->attributes,
            $properties
        );
    }

    /**
     * @param RestResourceActionAnnotation $annotation
     *
     * @return RestResourceAction
     */
    private function createAction(RestResourceActionAnnotation $annotation): RestResourceAction
    {
        return new RestResourceAction(
            $annotation->getName(),
            $annotation->getType(),
            $annotation->getMethod(),
            $annotation->getPath(),
            $annotation->getController(),
            $annotation->getRoute(),
            $annotation->getRequirements(),
            $annotation->getOptions(),
            $annotation->getDefaults(),
            $annotation->getCondition(),
            $annotation->getDescription(),
            $annotation->getValidationGroups(),
            $annotation->getSecurity()
        );
    }

    /**
     * @param RestResourceActionAnnotation $annotation
     *
     * @return RestResourceAction
     */
    private function createDefaultAction(string $type, string $name, string $method = null): RestResourceAction
    {
        return new RestResourceAction(
            $name,
            $type,
            $method
        );
    }
}
