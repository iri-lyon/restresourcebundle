<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResource
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 * @Target({"CLASS"})
 */
final class RestResource
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $manager;

    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return RestResource
     */
    public function setName(string $name): RestResource
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getManager(): ?string
    {
        return $this->manager;
    }

    /**
     * @param string $manager
     *
     * @return RestResource
     */
    public function setManager(string $manager): RestResource
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     *
     * @return RestResource
     */
    public function setAttributes(array $attributes): RestResource
    {
        $this->attributes = $attributes;

        return $this;
    }
}
