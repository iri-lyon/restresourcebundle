<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceFilter
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 * @Target({"PROPERTY"})
 */
class RestResourceFilter
{
    const TYPE_FILTER = 'filter';
    const TYPE_FILTER_MATCH = 'match';
    const TYPE_FILTER_MATCH_IN = 'match_in';
    const TYPE_FILTER_ORDER_BY = 'order';
    const TYPE_FILTER_RANGE = 'range';

    public $type = self::TYPE_FILTER;
}
