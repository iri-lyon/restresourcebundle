<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceFilterOrderBy
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 */
class RestResourceFilterOrderBy extends RestResourceFilter
{
    public $type = self::TYPE_FILTER_ORDER_BY;
}
