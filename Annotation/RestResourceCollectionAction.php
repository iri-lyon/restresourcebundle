<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceCollectionAction
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 */
final class RestResourceCollectionAction extends RestResourceAction
{
    protected $type = RestResourceAction::TYPE_COLLECTION;
}
