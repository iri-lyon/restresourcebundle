<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceItemAction
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 */
final class RestResourceItemAction extends RestResourceAction
{
    protected $type = RestResourceAction::TYPE_ITEM;
}
