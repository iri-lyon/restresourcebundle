<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceFilterRange
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 */
class RestResourceFilterRange extends RestResourceFilter
{
    public $type = self::TYPE_FILTER_RANGE;
}
