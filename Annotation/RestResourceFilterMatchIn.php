<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceFilterMatchIn
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 */
class RestResourceFilterMatchIn extends RestResourceFilter
{
    public $type = self::TYPE_FILTER_MATCH_IN;
}
