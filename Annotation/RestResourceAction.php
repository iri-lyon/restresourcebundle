<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceAction
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 * @Target({"CLASS"})
 */
class RestResourceAction
{

    const TYPE_ITEM = 'item';
    const TYPE_COLLECTION = 'collection';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $route;

    /**
     * @var array
     */
    protected $requirements = [];

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var array
     */
    protected $defaults = [];

    /**
     * @var string
     */
    protected $condition;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var callable|array
     */
    protected $validationGroups;

    /**
     * @var string
     */
    protected $security;


    /**
     * RestResourceAction constructor.
     *
     * @param array $data
     *
     * @throws \BadMethodCallException
     */
    public function __construct(array $data)
    {
        if (isset($data['value'])) {
            $data['name'] = $data['value'];
            unset ($data['value']);
        }

        foreach ($data as $key => $value) {
            $method = 'set'.str_replace('_', '', $key);
            if (!method_exists($this, $method)) {
                throw new \BadMethodCallException(sprintf(
                    'Unknow property "%s" on annotation "%s"',
                    $key,
                    get_class($this)
                ));
            }
            $this->$method($value);
        }
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return RestResourceAction
     */
    public function setName(string $name): RestResourceAction
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return RestResourceAction
     */
    public function setType(string $type): RestResourceAction
    {
        if (!in_array($type, [self::TYPE_COLLECTION, self::TYPE_ITEM])) {
            throw new \InvalidArgumentException(sprintf(
                'Action type "%s" is not valid. (%s, %s)',
                $type,
                self::TYPE_ITEM,
                self::TYPE_COLLECTION
            ));
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return RestResourceAction
     */
    public function setMethod(string $method): RestResourceAction
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return RestResourceAction
     */
    public function setPath(string $path): RestResourceAction
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getController(): ?string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     *
     * @return RestResourceAction
     */
    public function setController(string $controller): RestResourceAction
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string $route
     *
     * @return RestResourceAction
     */
    public function setRoute(string $route): RestResourceAction
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequirements(): array
    {
        return $this->requirements;
    }

    /**
     * @param array $requirements
     *
     * @return RestResourceAction
     */
    public function setRequirements(array $requirements): RestResourceAction
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return RestResourceAction
     */
    public function setOptions(array $options): RestResourceAction
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getDefaults(): array
    {
        return $this->defaults;
    }

    /**
     * @param array $defaults
     *
     * @return RestResourceAction
     */
    public function setDefaults(array $defaults): RestResourceAction
    {
        $this->defaults = $defaults;

        return $this;
    }

    /**
     * @return string
     */
    public function getCondition(): ?string
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     *
     * @return RestResourceAction
     */
    public function setCondition(string $condition): RestResourceAction
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return RestResourceAction
     */
    public function setDescription(string $description): RestResourceAction
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array|callable
     */
    public function getValidationGroups()
    {
        return $this->validationGroups;
    }

    /**
     * @param array|callable $validationGroups
     *
     * @return RestResourceAction
     */
    public function setValidationGroups($validationGroups): RestResourceAction
    {
        $this->validationGroups = $validationGroups;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecurity(): ?string
    {
        return $this->security;
    }

    /**
     * @param string $security
     *
     * @return RestResourceAction
     */
    public function setSecurity(string $security): RestResourceAction
    {
        $this->security = $security;

        return $this;
    }
}
