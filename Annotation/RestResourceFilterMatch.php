<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResourceFilterMatch
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 */
class RestResourceFilterMatch extends RestResourceFilter
{
    public $type = self::TYPE_FILTER_MATCH;
}
