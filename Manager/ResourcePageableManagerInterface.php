<?php

namespace GI\RestResourceBundle\Manager;

use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use Pagerfanta\Pagerfanta;


/**
 * Interface PageableManagerInterface
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Manager
 */
interface ResourcePageableManagerInterface
{
    /**
     * @param RestResourceMetadata  $resourceMetadata
     * @param array                 $criteria
     *
     * @return Pagerfanta
     */
    public function getPager(RestResourceMetadata $resourceMetadata, array $criteria = []);
}