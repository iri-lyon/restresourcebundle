<?php

namespace GI\RestResourceBundle\Manager;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Class AbstractResourceManager
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Manager
 */
abstract class AbstractResourceManager implements ResourceManagerInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $registry;

    /**
     * @var string
     */
    protected $class;

    /**
     * @param ManagerRegistry $registry
     * @param string          $class
     */
    public function __construct(ManagerRegistry $registry, $class)
    {
        $this->registry = $registry;
        $this->class    = $class;
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        $manager = $this->registry->getManagerForClass($this->class);

        if (!$manager) {
            throw new \RuntimeException(sprintf(
                'Unable to find the mapping information for the class %s.',
                $this->class
            ));
        }

        return $manager;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        $reflexion = new \ReflectionClass($this->class);
        return strtolower($reflexion->getShortName());
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * {@inheritdoc}
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->getRepository()->findOneBy($criteria, $orderBy);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create()
    {
        return new $this->class();
    }

    /**
     * {@inheritdoc}
     */
    public function save($entity, $andFlush = true, $force = false)
    {
        if (!$force) {
            $this->checkObject($entity);
        }

        $this->getObjectManager()->persist($entity);

        if ($andFlush) {
            $this->getObjectManager()->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove($entity, $andFlush = true)
    {
        $this->checkObject($entity);

        $this->getObjectManager()->remove($entity);

        if ($andFlush) {
            $this->getObjectManager()->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTableName()
    {
        return $this->getObjectManager()->getClassMetadata($this->class)->table['name'];
    }

    /**
     * Returns the related Object Repository.
     * @param string $class
     *
     * @return ObjectRepository
     */
    public function getRepository($class = null)
    {
        return $this->getObjectManager()->getRepository($class ?: $this->class);
    }

    /**
     * @param $object
     *
     * @throws \InvalidArgumentException
     */
    public function checkObject($object)
    {
        if (!$object instanceof $this->class) {
            throw new \InvalidArgumentException(sprintf(
                'Object must be instance of %s, %s given',
                $this->class,
                is_object($object) ? get_class($object) : gettype($object)
            ));
        }
    }


    /**
     * @param string $name
     * @param array  $args
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $args)
    {
        $repository = $this->getRepository();
        if (method_exists($repository, $name)) {
            return $repository->$name(...$args);
        }

        throw new \Exception(sprintf('Method %s not found in repository', $name));
    }
}
