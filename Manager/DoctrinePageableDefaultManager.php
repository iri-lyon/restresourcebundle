<?php

namespace GI\RestResourceBundle\Manager;

use GI\RestResourceBundle\Annotation\RestResourceFilter;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Metadata\RestResourcePropertyMetadata;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class DoctrinePageableDefaultManager
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Manager
 */
class DoctrinePageableDefaultManager extends AbstractResourceManager implements ResourcePageableManagerInterface
{
    /**
     * @var int
     */
    protected $maxPerPage = 10;

    /**
     * @param array $criterias
     *
     * @return Pagerfanta
     */
    public function getPager(RestResourceMetadata $resourceMetadata, array $criterias = [])
    {
        return $this->buildORMPager($resourceMetadata, $criterias);
    }

    /**
     * @param array $criteria
     *
     * @return Pagerfanta
     */
    protected function buildORMPager(RestResourceMetadata $resourceMetadata, array $criteria)
    {
        $queryBuilder = $this
            ->getRepository()
            ->createQueryBuilder('q');

        if ($criteria['_sort_field']) {
            $field = $criteria['_sort_field'];
            $sortDirection = 'DESC';

            if (substr($field, 0, 1) == '-') {
                $field = substr($field, 1);
                $sortDirection = 'ASC';
            }

            if ($this->hasPropertyFilter($resourceMetadata, $field, RestResourceFilter::TYPE_FILTER_ORDER_BY)) {
                $queryBuilder->orderBy('q.' . $field, $sortDirection);
            }
        } elseif ($this->hasPropertyFilter($resourceMetadata, 'createdAt', RestResourceFilter::TYPE_FILTER_ORDER_BY)) {
            $queryBuilder->orderBy('q.createdAt', 'DESC');
        }

        if ($criteria['_query']) {
            /**
             * @var string $name
             * @var RestResourcePropertyMetadata $property
             */
            foreach ($resourceMetadata->getProperties() as $name => $property) {
                if (isset($criteria['_query'][$name]) && !is_array($criteria['_query'][$name])) {
                    if ($property->hasFilter(RestResourceFilter::TYPE_FILTER_MATCH)) {
                        $value = str_replace('%', '', $criteria['_query'][$name]);
                        $queryBuilder
                            ->andWhere('q.'.$name. ' LIKE :'. $name)
                            ->setParameter($name, $value . '%');
                    } elseif ($property->hasFilter(RestResourceFilter::TYPE_FILTER_MATCH_IN)) {
                        $value = str_replace('%', '', $criteria['_query'][$name]);
                        $queryBuilder
                            ->andWhere('q.'.$name. ' LIKE :'. $name)
                            ->setParameter($name, '%' . $value . '%');
                    } elseif ($property->hasFilter(RestResourceFilter::TYPE_FILTER_RANGE)) {
                        $values = explode(',', $criteria['_query'][$name]);
                        if (count($values) === 1) {
                            $queryBuilder
                                ->andWhere('q.'.$name. ' >= :'. $name)
                                ->setParameter($name, $values[0]);
                        } else if (count($values) === 2 && $values[0] === '') {
                            $queryBuilder
                                ->andWhere('q.'.$name. '<= :'. $name)
                                ->setParameter($name, $values[1]);
                        } else if (count($values) === 2 && $values[0] !== '' && $values[1] !== '') {
                            $queryBuilder
                                ->andWhere('q.'.$name. ' BETWEEN :'. $name . '_start AND :' . $name . '_end')
                                ->setParameter($name.'_start', $values[0])
                                ->setParameter($name.'_end', $values[1]);
                        }
                    } elseif ($property->hasFilter(RestResourceFilter::TYPE_FILTER)) {
                        if ($criteria['_query'][$name] === 'false') {
                            $criteria['_query'][$name] = false;
                        } elseif ($criteria['_query'][$name] === 'true') {
                            $criteria['_query'][$name] = true;
                        }

                        if ($criteria['_query'][$name] === 'null') {
                            $queryBuilder
                                ->andWhere('q.'.$name. ' IS NULL');
                        } else {
                            $queryBuilder
                                ->andWhere('q.'.$name. ' = :'. $name)
                                ->setParameter($name, $criteria['_query'][$name]);

                        }
                    }
                }
            }
        }


        $pager = new Pagerfanta(new DoctrineORMAdapter($queryBuilder));
        $pager->setMaxPerPage($resourceMetadata->getAttribute('maxPerPage', $this->maxPerPage));

        return $pager;
    }

    /**
     * @param int $maxPerPage
     * @return DoctrinePageableDefaultManager
     */
    public function setMaxPerPage($maxPerPage)
    {
        $this->maxPerPage = $maxPerPage;

        return $this;
    }

    private function hasPropertyFilter(RestResourceMetadata $resourceMetadata, $name, $type)
    {
        if (!$property = $resourceMetadata->getProperty($name)){
            return false;
        }

        return $property->hasFilter($type);
    }
}
