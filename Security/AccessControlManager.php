<?php

namespace GI\RestResourceBundle\Security;

use GI\RestResourceBundle\Exception\RuntimeException;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\ExpressionLanguage;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * Class AccessControlManager
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Security
 */
class AccessControlManager
{
    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    /**
     * @var AuthenticationTrustResolverInterface
     */
    private $authenticationTrustResolver;

    /**
     * @var RoleHierarchyInterface
     */
    private $roleHierarchy;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * AccessControlManager constructor.
     *
     * @param ExpressionLanguage|null                   $expressionLanguage
     * @param AuthenticationTrustResolverInterface|null $authenticationTrustResolver
     * @param RoleHierarchyInterface|null               $roleHierarchy
     * @param TokenStorageInterface|null                $tokenStorage
     * @param AuthorizationCheckerInterface|null        $authorizationChecker
     */
    public function __construct(
        ExpressionLanguage $expressionLanguage,
        AuthenticationTrustResolverInterface $authenticationTrustResolver,
        RoleHierarchyInterface $roleHierarchy,
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->expressionLanguage = $expressionLanguage;
        $this->authenticationTrustResolver = $authenticationTrustResolver;
        $this->roleHierarchy = $roleHierarchy;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function isGranted(Request $request, $data = null)
    {
        /** @var RestResourceMetadata $metadata */
        $metadata = $request->attributes->get('resourceMetadata');
        if (!$metadata) {
            return;
        }

        try {
            $attributes = RequestAttributesExtractor::extractAttributes($request);
        } catch (RuntimeException $e) {
            return;
        }

        if (isset($attributes['collection_action_name'])) {
            $security = $metadata->getCollectionActionAttribute(
                $attributes['collection_action_name'],
                'security',
                null,
                true
            );
        } elseif (isset($attributes['item_action_name'])) {
            $security = $metadata->getItemActionAttribute(
                $attributes['item_action_name'],
                'security',
                null,
                true
            );
        }

        if ($security === null) {
            return;
        }

        $this->expressionLanguage->register('is_granted', function ($attributes, $object = 'null') {
            return sprintf('$auth_checker->isGranted(%s, %s)', $attributes, $object);
        }, function (array $variables, $attributes, $object = null) {
            return $variables['auth_checker']->isGranted($attributes, $object);
        });

        if (!$this->expressionLanguage->evaluate($security, $this->getVariables($request, $data))) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @copyright Fabien Potencier <fabien@symfony.com>
     *
     * @see https://github.com/symfony/symfony/blob/master/src/Symfony/Component/Security/Core/Authorization/Voter/ExpressionVoter.php
     */
    private function getVariables(Request $request, $data = null): array
    {
        $token = $this->tokenStorage->getToken();
        $roles = $this->roleHierarchy ?
            $this->roleHierarchy->getReachableRoles($token->getRoles()) :
            $token->getRoles();

        $variables = [
            'token' => $token,
            'user' => $token->getUser(),
            'object' => $data,
            'request' => $request,
            'roles' => array_map(function (Role $role) {
                return $role->getRole();
            }, $roles),
            'trust_resolver' => $this->authenticationTrustResolver,
            // needed for the is_granted expression function
            'auth_checker' => $this->authorizationChecker,
        ];
        // controller variables should also be accessible
        return array_merge($request->attributes->all(), $variables);
    }
}
