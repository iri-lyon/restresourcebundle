<?php

namespace GI\RestResourceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gi_rest_resource');

        $rootNode
            ->children()
                ->arrayNode('bundles')
                    ->prototype('scalar')->end()
                ->end()

                ->arrayNode('auto_generate_manager')
                ->canBeEnabled()
                ->children()
                    ->scalarNode('name')->defaultValue('app.%s.manager')->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

}