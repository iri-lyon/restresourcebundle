<?php

namespace GI\RestResourceBundle\DependencyInjection\Compiler;

use GI\RestResourceBundle\Manager\DefaultManager;
use GI\RestResourceBundle\Manager\DoctrinePageableDefaultManager;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceCollectionFactory;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceMetadataFactory;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

/**
 * Class GenerateManagerPass
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\DependencyInjection\Compiler
 */
class GenerateManagerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if (!$container->getParameter('rest.auto_generate_manager')) {
            return;
        }

        /** @var AnnotationRestResourceCollectionFactory $factory */
        $collectionFactory = $container->get('rest.metadata.rest_resource.collection_factory.annotation');
        /** @var AnnotationRestResourceMetadataFactory $metadataFactory */
        $metadataFactory = $container->get('rest.metadata.rest_resource.metadata_factory.annotation');

        foreach ($collectionFactory->create() as $resource) {
            $metadata = $metadataFactory->create($resource);

            if (!$container->has($metadata->getManager())) {
                if (class_exists('Symfony\Component\DependencyInjection\ChildDefinition')) {
                    $managerDefinition = new ChildDefinition('rest.orm.manager');
                } else {
                    $managerDefinition = new DefinitionDecorator('rest.orm.manager');
                }

                if ($metadata->getAttribute('pager') === true) {
                    $managerDefinition->setClass(DoctrinePageableDefaultManager::class);
                } else {
                    $managerDefinition->setClass(DefaultManager::class);
                }

                $managerDefinition->addArgument($resource);
                $container->setDefinition($metadata->getManager(), $managerDefinition);
            }
        }
    }
}
