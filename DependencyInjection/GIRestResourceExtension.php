<?php

namespace GI\RestResourceBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\DirectoryResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Finder\Finder;

/**
 * Class RestExtension
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\DependencyInjection
 */
class GIRestResourceExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $path = realpath($container->getParameter('kernel.root_dir').'/../src');
        $bundles = [];
        $bundles[] = [
            'dir' => $path,
            'ns' => 'App'.'\\'
        ];

        $availableBundles = $container->getParameter('kernel.bundles');
        foreach ($config['bundles'] as $bundle) {
            if (!array_key_exists($bundle, $availableBundles)) {
                throw new \RuntimeException(sprintf("Bundle %s not found.", $bundle));
            }
            $reflectionClass = new \ReflectionClass($availableBundles[$bundle]);
            $bundles[] = [
                'dir' => dirname($reflectionClass->getFileName()),
                'ns' => $reflectionClass->getNamespaceName().'\\'
            ];
        }

        $this->registerLoaders($container, $bundles);
        $this->loadManager($config, $container);
    }

    private function registerLoaders(ContainerBuilder $container, array $bundles)
    {
        $classes = [];
        $resourceClassDirectories = [];

        foreach ($bundles as $bundle) {

            $dirs = [];
            if (file_exists($entityDirectory = $bundle['dir'] . '/Entity')) {
                $resourceClassDirectories[] = $entityDirectory;
                $dirs[] = $entityDirectory;
                $container->addResource(new DirectoryResource($entityDirectory, '/\.php$/'));
            }

            if (file_exists($documentDirectory = $bundle['dir'] . '/Document')) {
                $resourceClassDirectories[] = $documentDirectory;
                $dirs[] = $documentDirectory;
                $container->addResource(new DirectoryResource($documentDirectory, '/\.php$/'));
            }

            if (empty($dirs)) {
                continue;
            }

            $finder = new Finder();
            $finder->files()->in($dirs)->name('*.php');
            foreach ($finder as $file) {
                $ns = $bundle['ns'].str_replace($bundle['dir'] . '/', '' , $file->getPath());
                $ns = str_replace('/', '\\', $ns);
                $classes[] = $ns . '\\' . $file->getBasename('.php');
            }
        }

        $container->setParameter('rest.resource_class_directories', $resourceClassDirectories);
        $container->getDefinition('rest.metadata.rest_resource.collection_factory.annotation')->addArgument($classes);
    }

    private function loadManager(array $config, ContainerBuilder $container)
    {

        $container->setParameter('rest.auto_generate_manager', !empty($config['auto_generate_manager']['enabled']));
        $container->setParameter('rest.auto_generate_manager.name', null);

        if (!empty($config['auto_generate_manager']['enabled'])) {
            $container
                ->getDefinition('rest.metadata.rest_resource.metadata_factory.annotation')
                ->replaceArgument(1, $config['auto_generate_manager']['name']);
        }
    }
}
