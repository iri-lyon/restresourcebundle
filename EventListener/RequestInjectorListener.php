<?php

namespace GI\RestResourceBundle\EventListener;

use GI\RestResourceBundle\Exception\RuntimeException;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class KernelRequestListener
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\EventListener
 */
class RequestInjectorListener
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * KernelRequestListener constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        try {
            $attributes = RequestAttributesExtractor::extractAttributes($request);
        } catch (RuntimeException $e) {
            return;
        }

        $resourceMetadataFactory = $this->container->get('rest.metadata.rest_resource.metadata_factory');
        $resourceMetadata = $resourceMetadataFactory->create($attributes['resource_class']);

        if ($resourceMetadata->getManager() && $this->container->has($resourceMetadata->getManager())) {
            $request->attributes->set('manager', $this->container->get($resourceMetadata->getManager()));
        }

        $request->attributes->set('resourceMetadata', $resourceMetadata);
    }
}
