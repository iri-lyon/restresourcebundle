<?php

namespace GI\RestResourceBundle\Controller;

use GI\RestResourceBundle\Event\ResourceEvent;
use GI\RestResourceBundle\Exception\ValidationException;
use GI\RestResourceBundle\Manager\ResourceManagerInterface;
use GI\RestResourceBundle\Manager\ResourcePageableManagerInterface;
use GI\RestResourceBundle\Metadata\RestResourceMetadata;
use GI\RestResourceBundle\ResourceEvents;
use GI\RestResourceBundle\Security\AccessControlManager;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use GI\RestResourceBundle\Serializer\RequestDeserializerBuilder;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractRestController
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Controller
 */
abstract class AbstractRestController implements ClassResourceInterface
{
    /**
     * @var AccessControlManager
     */
    protected $accessControlManager;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var RequestDeserializerBuilder
     */
    protected $requestDeserializerBuilder;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * AbstractRestController constructor.
     *
     * @param AccessControlManager $accessControlManager
     * @param TokenStorageInterface $tokenStorage
     * @param EventDispatcherInterface $dispatcher
     * @param RequestDeserializerBuilder $requestDeserializerBuilder
     * @param ValidatorInterface $validator
     */
    public function __construct(
        AccessControlManager $accessControlManager,
        TokenStorageInterface $tokenStorage,
        EventDispatcherInterface $dispatcher,
        RequestDeserializerBuilder $requestDeserializerBuilder,
        ValidatorInterface $validator
    ) {

        $this->accessControlManager = $accessControlManager;
        $this->tokenStorage = $tokenStorage;
        $this->dispatcher = $dispatcher;
        $this->requestDeserializerBuilder = $requestDeserializerBuilder;
        $this->validator = $validator;
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {

        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }

    /**
     * @param       $eventName
     * @param Event $event
     *
     * @return Event
     */
    protected function dispatch($eventName, Event $event)
    {
        return $this->dispatcher->dispatch($eventName, $event);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function search(Request $request, ResourceManagerInterface $manager, RestResourceMetadata $resourceMetadata)
    {
        $this->accessControlManager->isGranted($request);
        if ($manager instanceof ResourcePageableManagerInterface) {
            $criterias = [];
            $criterias['_sort_field'] = $request->query->get('order');
            $criterias['_query'] = $request->query->all();

            return $manager->getPager($resourceMetadata, $criterias);
        }

        return $manager->findAll();
    }

    /**
     * @param $id
     *
     * @return object|View
     */
    public function get(Request $request, ResourceManagerInterface $manager, $id)
    {
        $object = is_object($id) ? $id : $manager->find($id);
        if (!$object) {
            throw new NotFoundHttpException();
        }

        $manager->checkObject($object);
        $this->accessControlManager->isGranted($request, $object);

        return $object;
    }

    /**
     * @param Request $request
     * @param         $object
     *
     * @return mixed
     */
    public function post(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata,
        $object = null,
        $isFile = false
    ) {
        if ($object) {
            $manager->checkObject($object);
        }

        $data = $this
                ->requestDeserializerBuilder
                ->createObjectFromRequest($request, $manager->getClassName(), $object);

        $this->accessControlManager->isGranted($request, $data);

        $this->validateObject($request, $manager, $resourceMetadata, $data);

        $event = new ResourceEvent($data, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_CREATE),
            $event
        );
        $manager->save($data);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_CREATE),
            $event
        );

        return $data;
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return View
     */
    public function put(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata,
        $id
    ) {
        $object = is_object($id) ? $id : $manager->find($id);
        if (!$object) {
            throw new NotFoundHttpException();
        }

        $manager->checkObject($object);

        $data = $this
            ->requestDeserializerBuilder
            ->createObjectFromRequest($request, $manager->getClassName(), $object);

        $this->accessControlManager->isGranted($request, $object);

        $this->validateObject($request, $manager, $resourceMetadata, $data);

        $event = new ResourceEvent($data, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_UPDATE),
            $event
        );
        $manager->save($data);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_UPDATE),
            $event
        );

        return $data;
    }

    /**
     * @param Request                  $request
     * @param ResourceManagerInterface $manager
     * @param                          $id
     *
     * @return null
     */
    public function delete(Request $request, ResourceManagerInterface $manager, $id)
    {
        $object = is_object($id) ? $id : $manager->find($id);
        if (!$object) {
            throw new NotFoundHttpException();
        }

        $manager->checkObject($object);
        $this->accessControlManager->isGranted($request, $object);

        $event = new ResourceEvent($object, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_DELETE),
            $event
        );

        $manager->remove($object);

        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_DELETE),
            $event
        );

        return null;
    }

    /**
     * @param Request $request
     * @param RestResourceMetadata $resourceMetadata
     * @param $data
     *
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    protected function validateObject(
        Request $request,
        ResourceManagerInterface $manager,
        RestResourceMetadata $resourceMetadata,
        $data
    ) {
        $event = new ResourceEvent($data, $manager, $request);
        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::PRE_VALIDATE),
            $event
        );

        $attributes = RequestAttributesExtractor::extractAttributes($request);

        if (isset($attributes['collection_action_name'])) {
            $validationGroups = $resourceMetadata->getCollectionActionAttribute(
                $attributes['collection_action_name'],
                'validation_groups'
            );
        } else {
            $validationGroups = $resourceMetadata->getItemActionAttribute(
                $attributes['item_action_name'],
                'validation_groups'
            );
        }

        if (!$validationGroups) {
            // Fallback to the resource
            $validationGroups = $resourceMetadata->getAttributes()['validation_groups'] ?? null;
        }

        if (is_callable($validationGroups)) {
            $validationGroups = call_user_func_array($validationGroups, [$data]);
        }

        $violations = $this->validator->validate($data, null, $validationGroups);
        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }

        $this->dispatch(
            ResourceEvents::build($manager->getShortName(), ResourceEvents::POST_VALIDATE),
            $event
        );
    }

    /**
     * @param $method
     *
     * @return MethodNotAllowedHttpException
     */
    protected function createMethodNotAllowedException($method)
    {
        return new MethodNotAllowedHttpException(['GET', 'DELETE'], $method . ' method is not allowed.');
    }
}
