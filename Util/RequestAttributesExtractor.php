<?php

namespace GI\RestResourceBundle\Util;

use GI\RestResourceBundle\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestAttributesExtractor
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Util
 */
class RequestAttributesExtractor
{

    public static function extractAttributes(Request $request)
    {
        if (!$request->attributes->has('_rest_resource_class')) {
            throw new RuntimeException('The request is not a rest resource');
        }

        $result = [
            'resource_class' => $request->attributes->get('_rest_resource_class'),
            'resource_name' => $request->attributes->get('_rest_resource_name')
        ];

        $collectionActionName = $request->attributes->get('_rest_collection_action_name');
        $itemActionName = $request->attributes->get('_rest_item_action_name');

        if ($collectionActionName) {
            $result['collection_action_name'] = $collectionActionName;
        } elseif ($itemActionName) {
            $result['item_action_name'] = $itemActionName;
        } else {
            throw new RuntimeException(
                'One of the request attribute "_api_collection_action_name" or 
                "_rest_item_action_name" must be defined.'
            );
        }

        return $result;
    }
}
