<?php

namespace GI\RestResourceBundle\Pagination;

use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use GI\RestResourceBundle\Pagination\Exception\HttpRangeNotSatisfiable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


/**
 * Class RangeHeaderTrait
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Pagination
 */
trait RangeHeaderTrait
{

    public function getRequestRange(Request $request, $limit = 10, $maxRange = 100)
    {
        $start = 0;
        $end = $limit;

        $range = $request->headers->get('Range');
        if ($range) {
            if (!preg_match('/^([0-9]+)\-([0-9]+)$/', $range, $matches)) {
                throw new BadRequestHttpException('Range header is not valid');
            }
            $start = $matches[1];
            $end = $matches[2];
            $limit = ($end-$start) + 1;

            if (($end-$start) > $maxRange) {
                throw new HttpRangeNotSatisfiable();
            }

            if (($start%$limit) !== 0) {
                throw new HttpRangeNotSatisfiable();
            }
        }

        return new Range($start, $end, $limit);
    }
}